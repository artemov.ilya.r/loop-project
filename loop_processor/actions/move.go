package actions

import (
	"fmt"
	. "github.com/ajstarks/svgo"
	"math"
	"strconv"
	. "system/loop_processor/state"
)

type MoveAction struct {
	Action
	Length       float64
	currentColor string
}

func (action *MoveAction) Process(currentState State) State {
	return State{
		X:     currentState.X + int(action.Length*math.Cos(currentState.Angle)),
		Y:     currentState.Y + int(action.Length*math.Sin(currentState.Angle)),
		Angle: currentState.Angle,
		Color: currentState.Color,
	}
}

func (action *MoveAction) Apply(state State, canvas SVG) State {
	nextState := action.Process(state)
	if state.Color != "" {
		action.currentColor = state.Color
	} else {
		action.currentColor = "black"
	}
	pathCords := fmt.Sprintf("M%d,%d L%d,%d", state.X, -state.Y, nextState.X, -nextState.Y)
	pathParams := fmt.Sprintf("stroke-width=\"5\" stroke=\"%s\"", action.currentColor)
	canvas.Path(pathCords, pathParams)
	return nextState
}

func NewMoveAction(args []string) Action {
	length, _ := strconv.ParseFloat(args[1], 64)
	return &MoveAction{Length: length}
}
