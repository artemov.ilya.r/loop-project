package actions

import (
	. "github.com/ajstarks/svgo"
	. "system/loop_processor/state"
)

type Action interface {
	Apply(state State, canvas SVG) State
	Process(state State) State
	New([]string) Action
}
