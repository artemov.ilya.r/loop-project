package actions

import (
	. "github.com/ajstarks/svgo"
	. "system/loop_processor/state"
)

type ColorAction struct {
	Action
	Color string
}

func (colorAction *ColorAction) Process(state State) State {
	return State{
		X:     state.X,
		Y:     state.Y,
		Angle: state.Angle,
		Color: colorAction.Color,
	}
}

func (colorAction *ColorAction) Apply(state State, canvas SVG) State {
	return colorAction.Process(state)
}

func NewColorAction(args []string) Action {
	color := args[1]
	return &ColorAction{Color: color}
}
