package actions

import . "regexp"

type Pattern struct {
	regex  *Regexp
	action func([]string) Action
}

var Mapping = []Pattern{
	{
		MustCompile(`move\(([\d+]*)\)`),
		func(args []string) Action {
			return NewMoveAction(args)
		},
	},
	{
		MustCompile(`rotate\((\d+|-\d+)\)`),
		func(args []string) Action {
			return NewRotateAction(args)
		},
	},
	{
		MustCompile(`color\((#\w+)\)`),
		func(args []string) Action {
			return NewColorAction(args)
		},
	},
}

func CreateAction(str string) Action {
	for _, mapVariant := range Mapping {
		matches := mapVariant.regex.FindStringSubmatch(str)
		if len(matches) == 0 {
			continue
		}
		return mapVariant.action(matches)
	}
	// TODO: raise error
	return nil
}
