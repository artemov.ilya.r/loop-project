package actions

import (
	. "github.com/ajstarks/svgo"
	"math"
	"strconv"
	. "system/loop_processor/state"
)

type RotateAction struct {
	Action
	Angle float64
}

func (rotateAction *RotateAction) Process(state State) State {
	return State{
		X:     state.X,
		Y:     state.Y,
		Angle: state.Angle + rotateAction.Angle*math.Pi/180.0,
		Color: state.Color,
	}
}

func (rotateAction *RotateAction) Apply(state State, canvas SVG) State {
	return rotateAction.Process(state)
}

func NewRotateAction(args []string) Action {
	angle, _ := strconv.ParseFloat(args[1], 64)
	return &RotateAction{Angle: angle}
}
