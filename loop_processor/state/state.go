package state

type State struct {
	X     int
	Y     int
	Angle float64
	Color string
}
