package drawer

import (
	"fmt"
	svg "github.com/ajstarks/svgo"
	"io/ioutil"
	"os"
	. "system/loop_processor/sequence"
	. "system/loop_processor/state"
	. "system/utils"
)

func Draw(sequence Sequence) string {
	tokenList := sequence.GetResult()
	fileName := "result.svg"
	fo, err := os.Create(fileName)
	if err != nil {
		panic(err)
	}
	width := 500
	height := 500
	canvas := svg.New(fo)
	canvas.Startview(width, height, 0, 0, 0, 0)
	previousState := State{
		X:     0.0,
		Y:     0.0,
		Angle: 0.0,
	}
	minX, maxX, minY, maxY := 0, 0, 0, 0
	for _, action := range tokenList {
		nextState := action.Apply(previousState, *canvas)
		previousState = nextState
		if nextState.X > maxX {
			maxX = nextState.X
		}
		if nextState.X < minX {
			minX = nextState.X
		}
		if nextState.Y > maxY {
			maxY = nextState.Y
		}
		if nextState.Y < minY {
			minY = nextState.Y
		}
	}
	canvas.End()
	err = replaceViewBox(fileName, fmt.Sprintf("%d %d %d %d", minX, -maxY, Abs(minX)+Abs(maxX), Abs(minY)+Abs(maxY)))
	if err != nil {
		return ""
	}
	return fileName
}

func replaceViewBox(filePath, newViewBox string) error {
	// Чтение файла
	data, err := ioutil.ReadFile(filePath)
	if err != nil {
		return err
	}

	// Найти байтовый паттерн для текущего значения атрибута viewBox
	viewBoxPattern := []byte(`viewBox="`)
	startIndex := -1
	for i := 0; i < len(data)-len(viewBoxPattern); i++ {
		if string(data[i:i+len(viewBoxPattern)]) == string(viewBoxPattern) {
			startIndex = i + len(viewBoxPattern)
			break
		}
	}
	if startIndex == -1 {
		return fmt.Errorf("Атрибут viewBox не найден в файле %s", filePath)
	}

	// Найти индексы начала и конца текущего значения атрибута viewBox
	endIndex := -1
	for i := startIndex; i < len(data); i++ {
		if data[i] == '"' {
			endIndex = i
			break
		}
	}
	if endIndex == -1 {
		return fmt.Errorf("Неправильный формат атрибута viewBox в файле %s", filePath)
	}

	// Заменить значение атрибута viewBox на новое значение
	copy(data[startIndex:endIndex], []byte(newViewBox))

	// Запись изменений в файл
	err = ioutil.WriteFile(filePath, data, 0644)
	if err != nil {
		return err
	}
	return nil
}
