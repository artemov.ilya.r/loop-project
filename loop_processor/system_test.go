package loop_processor

import (
	actions2 "system/loop_processor/actions"
	"system/loop_processor/sequence"
	"testing"
)

func TestRawSequence(t *testing.T) {
	seq := sequence.Sequence{}
	seq.Init(
		"f",
		map[string]string{
			"f": "f+fof+f",
		},
		map[string]actions2.Action{
			"f":   &actions2.MoveAction{Length: 100.0},
			"+":   &actions2.RotateAction{Angle: 90.0},
			"-":   &actions2.RotateAction{Angle: -90.0},
			"fof": &actions2.RotateAction{Angle: -90.0},
		},
		2,
	)
	seq.Process()
	exceptedResult := "f+fof+f+fof+f+fof+f"
	if seq.StringResult() != exceptedResult {
		t.Error("Wrong iterate")
	}

	seq = sequence.Sequence{}
	seq.Init(
		"f",
		map[string]string{
			"f": "f+f-f+f",
		},
		map[string]actions2.Action{
			"f": &actions2.MoveAction{Length: 100.0},
			"+": &actions2.RotateAction{Angle: 90.0},
			"-": &actions2.RotateAction{Angle: -90.0},
		},
		3,
	)
	seq.Process()
	//println(sequence.StringResult())
	exceptedResult = "f+f-f+f+f+f-f+f-f+f-f+f+f+f-f+f+f+f-f+f+f+f-f+f-f+f-f+f+f+f-f+f-f+f-f+f+f+f-f+f-f+f-f+f+f+f-f+f+f+f-f+f+f+f-f+f-f+f-f+f+f+f-f+f"
	if seq.StringResult() != exceptedResult {
		t.Error("Wrong iterate")
	}

	seq = sequence.Sequence{}
	seq.Init(
		"fc",
		map[string]string{
			"f": "f+c-f",
			"c": "c+f-c",
		},
		map[string]actions2.Action{
			"f": &actions2.MoveAction{Length: 100.0},
			"c": &actions2.RotateAction{Angle: 60.0},
			"+": &actions2.RotateAction{Angle: 90.0},
			"-": &actions2.RotateAction{Angle: -90.0},
		},
		2,
	)
	seq.Process()
	//println(seq.StringResult())
	exceptedResult = "f+c-f+c+f-c-f+c-fc+f-c+f+c-f-c+f-c"
	if seq.StringResult() != exceptedResult {
		t.Error("Wrong iterate")
	}
}
