package sequence

import (
	"golang.org/x/exp/slices"
	"sort"
	"system/loop_processor/actions"
	. "system/utils"
)

type Sequence struct {
	axiom          string
	translation    map[string]string
	actions        map[string]actions.Action
	count          int
	translationMap map[string]int
	tokenMap       map[int][]int
	parsedAxiom    []int
	result         []int
}

func (seq *Sequence) Init(axiom string, translation map[string]string, actions map[string]actions.Action, count int) {
	seq.axiom = axiom
	seq.translation = translation
	seq.actions = actions
	seq.count = count

	seq.translationMap = map[string]int{}
	seq.tokenMap = map[int][]int{}

	i := 0
	for key, _ := range actions {
		seq.translationMap[key] = i
		i++
	}
	i += 1000
	for key, _ := range translation {
		seq.translationMap[key] = i
		i++
	}
	keys := Keys(seq.translationMap)
	for key, value := range translation {
		newKey := seq.translationMap[key]
		splitedValue := splitByNSeps(value, keys)
		newValues := []int{}
		for _, item := range splitedValue {
			newValues = append(newValues, seq.translationMap[item])
		}
		seq.tokenMap[newKey] = newValues
	}
	startString := splitByNSeps(seq.axiom, keys)
	for _, value := range startString {
		seq.parsedAxiom = append(seq.parsedAxiom, seq.translationMap[value])
	}
}

func splitByNSeps(data string, keys []string) []string {
	if len(data) == 1 && slices.Contains(keys, data) {
		return []string{data}
	}
	sort.Slice(keys, func(i, j int) bool {
		return len(keys[i]) > len(keys[j])
	})

	slices := []struct{ start, end int }{}
	for _, key := range keys {
		currentLen := len(key)
		for i := 0; i < len(data); i++ {
			if i+currentLen > len(data) {
				break
			}
			if data[i:i+currentLen] != key {
				continue
			}
			skipPair := false
			for _, slice := range slices {
				if slice.start <= i && i <= slice.end && slice.start <= i+currentLen && i+currentLen <= slice.end {
					skipPair = true
				}
			}
			if skipPair {
				skipPair = false
				continue
			}
			slices = append(slices, struct{ start, end int }{i, i + currentLen})
		}
	}

	res := []string{}
	sort.Slice(slices, func(i, j int) bool {
		return slices[i].start < slices[j].start
	})
	for _, s := range slices {
		res = append(res, data[s.start:s.end])
	}
	return res
}

func (seq *Sequence) Process() []int {
	tmpResult := seq.parsedAxiom
	keys := Keys(seq.tokenMap)
	for i := 0; i < seq.count; i++ {
		newResult := []int{}
		for _, value := range tmpResult {
			if slices.Contains(keys, value) {
				newResult = append(newResult, seq.tokenMap[value]...)
			} else {
				newResult = append(newResult, value)
			}
		}
		tmpResult = newResult
	}
	seq.result = tmpResult
	return tmpResult
}

func (seq Sequence) GetResult() []actions.Action {
	result := []actions.Action{}
	reverseTranslation := map[int]string{}
	for key, value := range seq.translationMap {
		reverseTranslation[value] = key
	}
	for _, value := range seq.result {
		result = append(result, seq.actions[reverseTranslation[value]])
	}
	return result
}

func (seq Sequence) StringResult() string {
	reverseTranslation := map[int]string{}
	for key, value := range seq.translationMap {
		reverseTranslation[value] = key
	}
	resultStr := ""
	for _, value := range seq.result {
		resultStr += reverseTranslation[value]
	}
	return resultStr
}
