# db CRUD module

---

## Usage


1. Define strcut with field as in database table:
   - Name isn`t important
   - Field order is important
   - Add tag mapping with your custom tag (```db``` for example) to database field names 
```go
type Collection struct {
	Pk       int      `db:"pk"`
	Name     string   `db:"name"`
	ParentId null.Int `db:"parent"`
}
```

2. Create CRUD interface:
    - TableName - database table name
    - ModelType - type of struct
    - PkFieldName - name of primary key
```go
func TableCRUD() db.CrudI {
	return db.CrudBase{
		TableName:   "TestTable",
		ModelType:   reflect.TypeOf(TestTable{}),
		PkFieldName: "pk",
		StructTag:   "db",
	}
}

```

Examples:

```go
// define
newCollection := Collection{Name: "MyCollection"}
CrudInterface := CollectionCRUD()
// Create
err, insertedPk := CrudInterface.Create(newCollection)
// Read
err, readObject := CrudInterface.Read(insertedPk)
collection := readObject.(Collection)
// Update
updateObject := Collection{Name: "NewName"}
err, _ = CrudInterface.Update(updateObject)
// Delete
CrudInterface.Delete(insertedPk)
```