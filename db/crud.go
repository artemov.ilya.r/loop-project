package db

import (
	"database/sql"
	"reflect"
	"system/utils"
)

// TODO: generate db tag from CrudBase.StructTag
type pkRow struct {
	Pk int `db:"pk"`
}

type CrudI interface {
	Create(data interface{}) (error, int)
	Update(data interface{}) (error, interface{})
	Delete(pk int) error
	Read(pk int) (error, interface{})
	ToIdentifier(key string) string
	EmptyObject() interface{}
	SetPkField(object interface{}, value int)
	FromMap(object interface{}, data map[string]interface{})
	setField(object interface{}, key string, value interface{})
}

type CrudBase struct {
	CrudI
	TableName   string
	ModelType   reflect.Type
	PkFieldName string
	StructTag   string
}

type ListFilter interface {
	List(filter interface{}) []interface{}
}

func (receiver CrudBase) ToIdentifier(key string) string {
	return `"` + key + `"`
}

func (receiver CrudBase) Create(data interface{}) (error, int) {
	args := receiver.getFieldsFromStruct(data)
	delete(args, receiver.PkFieldName)
	query := receiver.createQuery(utils.Keys(args))
	rows, err := db.NamedQuery(query, args)
	defer rows.Close()
	for rows.Next() {
		obj := pkRow{}
		rows.StructScan(&obj)
		if err != nil {
			return err, -1
		}
		return nil, obj.Pk
	}
	return sql.ErrNoRows, -1
}

func (receiver CrudBase) Read(pk int) (error, interface{}) {
	query := receiver.readQuery()
	params := map[string]interface{}{"pk": pk}
	rows, err := db.NamedQuery(query, params)
	if err != nil {
		return err, nil
	}
	defer rows.Close()
	for rows.Next() {
		reflectObject := reflect.New(receiver.ModelType).Elem()
		fields := receiver.getFieldsFromModel(reflectObject)
		err := rows.Scan(fields...)
		if err != nil {
			return err, nil
		}
		return nil, reflectObject.Interface()
	}
	return sql.ErrNoRows, nil
}

func (receiver CrudBase) Update(data interface{}) (error, interface{}) {
	args := receiver.getFieldsFromStruct(data)
	query := receiver.updateQuery(utils.Keys(args))
	rows, err := db.NamedQuery(query, args)
	if err != nil {
		return err, nil
	}
	defer rows.Close()
	for rows.Next() {
		return nil, data
	}
	return sql.ErrNoRows, nil
}

func (receiver CrudBase) Delete(pk int) error {
	query := receiver.deleteQuery()
	args := map[string]interface{}{receiver.PkFieldName: pk}
	rows, err := db.NamedQuery(query, args)
	defer rows.Close()
	for rows.Next() {
	}
	if err != nil {
		return err
	}
	return nil
}

func (receiver CrudBase) EmptyObject() interface{} {
	return reflect.New(receiver.ModelType).Elem().Addr().Interface()
}

func (receiver CrudBase) FromMap(object interface{}, data map[string]interface{}) {
	// TODO: process error
	for key, value := range data {
		receiver.setField(object, key, value)
	}
}

func (receiver CrudBase) SetPkField(object interface{}, value int) {
	receiver.setField(object, receiver.PkFieldName, value)
}

func (receiver CrudBase) setField(object interface{}, key string, value interface{}) {
	robject := reflect.ValueOf(object)
	rvalue := reflect.ValueOf(value)
	if robject.Type().Kind() == reflect.Pointer {
		robject = robject.Elem()
	}
	for i := 0; i < receiver.ModelType.NumField(); i++ {
		if receiver.ModelType.Field(i).Tag.Get(receiver.StructTag) == key {
			robject.Field(i).Set(rvalue)
			return
		}
	}
}

func (receiver CrudBase) getFieldsFromModel(reflectObject reflect.Value) []interface{} {
	columnNum := reflectObject.NumField()
	fields := make([]interface{}, columnNum)
	for i := 0; i < columnNum; i++ {
		field := reflectObject.Field(i)
		fields[i] = field.Addr().Interface()
	}
	return fields
}

func (receiver CrudBase) getFieldsFromStruct(data interface{}) map[string]interface{} {
	fieldNum := receiver.ModelType.NumField()
	result := map[string]interface{}{}
	reflectValue := reflect.ValueOf(data)
	if reflectValue.Type().Kind() == reflect.Pointer {
		reflectValue = reflectValue.Elem()
	}
	for i := 0; i < fieldNum; i++ {
		fieldName := receiver.ModelType.Field(i).Tag.Get(receiver.StructTag)
		fieldValue := reflectValue.Field(i).Interface()
		result[fieldName] = fieldValue
	}
	return result
}

func (receiver CrudBase) createQuery(params []string) string {
	tableName := receiver.ToIdentifier(receiver.TableName)
	query := `INSERT INTO ` + tableName + `(`
	paramCount := len(params)
	for i := 0; i < paramCount; i++ {
		if i == paramCount-1 {
			query += receiver.ToIdentifier(params[i]) + ")\n"
		} else {
			query += receiver.ToIdentifier(params[i]) + ", "
		}
	}
	query += "VALUES ("
	for i := 0; i < paramCount; i++ {
		if i == paramCount-1 {
			query += ":" + params[i] + ")\n"
		} else {
			query += ":" + params[i] + ", "
		}
	}
	query += "RETURNING pk;"
	return query
}

func (receiver CrudBase) readQuery() string {
	tableName := receiver.ToIdentifier(receiver.TableName)
	pkField := receiver.ToIdentifier(receiver.PkFieldName)
	return `
		SELECT *
		FROM ` + tableName + `
		WHERE ` + pkField + ` = :pk
		LIMIT 1;
	`
}

func (receiver CrudBase) updateQuery(params []string) string {
	tableName := receiver.ToIdentifier(receiver.TableName)
	pkField := receiver.ToIdentifier(receiver.PkFieldName)
	query := "UPDATE " + tableName + "\nSET\n"
	for i, param := range params {
		if param == receiver.PkFieldName {
			continue
		}
		if i == len(params)-1 {
			query += "\n\t" + receiver.ToIdentifier(param) + " = :" + param
		} else {
			query += "\n\t" + receiver.ToIdentifier(param) + " = :" + param + ","
		}
	}
	query += "\nWHERE " + pkField + " = :" + receiver.PkFieldName
	return query
}

func (receiver CrudBase) deleteQuery() string {
	tableName := receiver.ToIdentifier(receiver.TableName)
	pkField := receiver.ToIdentifier(receiver.PkFieldName)
	return "DELETE FROM " + tableName + " WHERE " + pkField + " = :" + receiver.PkFieldName
}
