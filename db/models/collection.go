package database

import (
	"database/sql"
	"reflect"
	. "system/db"
)

type Collection struct {
	Pk       int           `db:"pk" json:"pk,omitempty"`
	Name     string        `db:"name" json:"name,omitempty"`
	ParentId sql.NullInt64 `db:"parent" json:"parent_id,omitempty"`
}

type CollectionFilter struct {
	Keys   []int
	Names  []string
	Parent []int
}

type collectionCRUD struct {
	CrudBase
}

func CollectionCRUD() CrudI {
	return collectionCRUD{
		CrudBase{
			TableName:   "collection",
			ModelType:   reflect.TypeOf(Collection{}),
			StructTag:   "db",
			PkFieldName: "pk",
		},
	}
}
