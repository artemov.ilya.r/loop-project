package database

import (
	"reflect"
	. "system/db"
)

type Loop struct {
	Pk         int    `db:"pk" json:"pk,omitempty"`
	Data       string `db:"name" json:"data,omitempty"`
	Image      string `db:"parent" json:"image,omitempty"`
	Name       string `db:"name" json:"name,omitempty"`
	Collection int    `db:"collection" json:"collection,omitempty"`
}

type loopCrud struct {
	CrudBase
}

func LoopCRUD() CrudI {
	return loopCrud{
		CrudBase{
			TableName:   "loop",
			ModelType:   reflect.TypeOf(Loop{}),
			StructTag:   "db",
			PkFieldName: "pk",
		},
	}
}
