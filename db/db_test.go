package db

import (
	"database/sql"
	"github.com/joho/godotenv"
	"log"
	"os"
	"reflect"
	"testing"
)

type TestTable struct {
	Pk        int           `db:"pk"`
	TextField string        `db:"text_field"`
	IntField  sql.NullInt64 `db:"int_field"`
}

func TableCRUD() CrudI {
	return CrudBase{
		TableName:   "TestTable",
		ModelType:   reflect.TypeOf(TestTable{}),
		PkFieldName: "pk",
		StructTag:   "db",
	}
}

func beforeRun() {
	err := godotenv.Load(`../.env`)
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	testDatabasePath := os.Getenv("DB_TEST_PATH")
	GetConnection(testDatabasePath)
}

func TestCRUD(t *testing.T) {
	beforeRun()
	object := TestTable{
		IntField:  sql.NullInt64{},
		TextField: "Test",
	}
	// ########################## Create ##########################
	err, pk := TableCRUD().Create(object)
	if err != nil {
		t.Error("CREATE method failed")
		err.Error()
	}
	object.Pk = pk
	// ########################### Read ###########################
	err, readData := TableCRUD().Read(object.Pk)
	if err != nil {
		t.Error("READ method failed")
		err.Error()
	}
	readObject := readData.(TestTable)
	if readObject.Pk != object.Pk ||
		readObject.IntField != object.IntField ||
		readObject.TextField != object.TextField {
		t.Error("READ method failed")
	}
	// ########################## Update ##########################
	updateObject := TestTable{
		Pk:        object.Pk,
		TextField: "NEW_VALUE",
	}
	err, _ = TableCRUD().Update(updateObject)
	err, readData = TableCRUD().Read(updateObject.Pk)
	readObject = readData.(TestTable)
	if err != nil || readObject.TextField != updateObject.TextField {
		t.Error("UPDATE method failed")
	}
	// ########################## Delete ##########################
	err = TableCRUD().Delete(object.Pk)
	if err != nil {
		t.Error("DELETE method failed")
	}
	err, _ = TableCRUD().Read(object.Pk)
	if err == nil {
		t.Error("DELETE method failed")
	}
}
