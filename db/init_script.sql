DROP TABLE IF EXISTS loop;
DROP TABLE IF EXISTS collection;

CREATE TABLE loop
(
    id    INT PRIMARY KEY NOT NULL,
    data  json            NOT NULL,
    image TEXT,
    name  TEXT
);

CREATE TABLE collection
(
    id     INT PRIMARY KEY NOT NULL,
    name   TEXT            NOT NULL,
    parent INT,
    FOREIGN KEY (parent) REFERENCES collection (parent)
);

