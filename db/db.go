package db

import (
	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
)

var db *sqlx.DB

func GetConnection(databasePath string) *sqlx.DB {
	if db == nil {
		db = createConnection(databasePath)
	}
	return db
}

func createConnection(databasePath string) *sqlx.DB {
	db, err := sqlx.Connect("sqlite3", databasePath)

	if err != nil {
		panic(err)
	}
	return db
}
