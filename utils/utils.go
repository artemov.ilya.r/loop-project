package utils

// Keys get all map keys
func Keys[M ~map[K]V, K comparable, V any](m M) []K {
	r := make([]K, 0, len(m))
	for k := range m {
		r = append(r, k)
	}
	return r
}

// Keys get all map keys
func Values[M ~map[K]V, K comparable, V any](m M) []V {
	r := make([]V, 0, len(m))
	for _, v := range m {
		r = append(r, v)
	}
	return r
}

func Abs(n int) int {
	if n > 0 {
		return n
	} else {
		return -n
	}
}

//func GetStructFieldByTag(dataType reflect.Type, tag string, tagName string) (bool, reflect.StructField) {
//	for i := 0; i < dataType.NumField(); i++ {
//		field := dataType.Field(i)
//		if field.Tag.Get(tag) == tagName {
//			return true, field
//		}
//	}
//	return false, reflect.StructField{}
//}

// Remove by value from slice of comparable items
func Remove[T comparable](l []T, item T) []T {
	for i, other := range l {
		if other == item {
			return append(l[:i], l[i+1:]...)
		}
	}
	return l
}
