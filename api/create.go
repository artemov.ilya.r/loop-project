package api

import (
	. "system/loop_processor/actions"
	. "system/loop_processor/drawer"
	. "system/loop_processor/sequence"
)

func Create(data LoopData) string {
	translationRules := map[string]string{}
	for key, value := range data.Translation {
		if key != "axiom" {
			translationRules[key] = value
		}
	}
	actions := map[string]Action{}
	for key, value := range data.Actions {
		actions[key] = CreateAction(value)
	}
	seq := Sequence{}
	seq.Init(
		data.Translation["axiom"],
		translationRules,
		actions,
		data.Count,
	)
	seq.Process()
	return Draw(seq)
}
