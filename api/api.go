package api

import (
	"github.com/gofiber/fiber/v2"
	"strconv"
	"system/db"
	. "system/db/models"
)

func RunAPI() {
	app := fiber.New()
	apiParts := map[string]db.CrudI{
		"collection": CollectionCRUD(),
		"loop":       LoopCRUD(),
	}
	println("API PARTS:")
	for name, crudInterface := range apiParts {
		addRestModelAPI(app, name, crudInterface)
	}
	app.Post("/api/create", func(c *fiber.Ctx) error {
		data := LoopData{}
		if err := c.BodyParser(&data); err != nil {
			return err
		}
		Create(data)
		return c.JSON(data)
	})
	app.Get("/", func(c *fiber.Ctx) error {
		result := map[string]bool{
			"ok": true,
		}
		return c.JSON(result)
	})
	app.Listen(":3000")
}

func addRestModelAPI(app *fiber.App, modelName string, crudInterface db.CrudI) {
	addGetModelRoute(app, modelName, crudInterface)
	addPostModelRoute(app, modelName, crudInterface)
	addPutModelRoute(app, modelName, crudInterface)
	addDelModelRoute(app, modelName, crudInterface)
}

func addGetModelRoute(app *fiber.App, modelName string, crudInterface db.CrudI) {
	path := "/api/" + modelName
	idPath := path + "/:id"
	app.Get(idPath, func(c *fiber.Ctx) error {
		idParam := c.Params("id")
		objectId, err := strconv.Atoi(idParam)
		if err != nil {
			return err
		}
		err, data := crudInterface.Read(objectId)
		if err != nil {
			return err
		}
		return c.JSON(data)
	})
	println("\t" + idPath)
}

func addPostModelRoute(app *fiber.App, modelName string, crudInterface db.CrudI) {
	path := "/api/" + modelName
	app.Post(path, func(c *fiber.Ctx) error {
		data := crudInterface.EmptyObject()
		if err := c.BodyParser(&data); err != nil {
			return err
		}
		err, insertedPk := crudInterface.Create(data)
		if err != nil {
			return err
		}
		crudInterface.SetPkField(data, insertedPk)
		return c.JSON(data)
	})
	println("\t" + path)
}

func addPutModelRoute(app *fiber.App, modelName string, crudInterface db.CrudI) {
	path := "/api/" + modelName
	app.Put(path, func(c *fiber.Ctx) error {
		data := crudInterface.EmptyObject()
		if err := c.BodyParser(&data); err != nil {
			return err
		}
		err, _ := crudInterface.Update(data)
		if err != nil {
			return err
		}
		return c.JSON(data)
	})
	println("\t" + path)
}

func addDelModelRoute(app *fiber.App, modelName string, crudInterface db.CrudI) {
	path := "/api/" + modelName
	idPath := path + "/:id"
	app.Delete(idPath, func(c *fiber.Ctx) error {
		idParam := c.Params("id")
		objectId, err := strconv.Atoi(idParam)
		if err != nil {
			return err
		}
		err = crudInterface.Delete(objectId)
		if err != nil {
			return err
		}
		return c.JSON(objectId)
	})
	println("\t" + idPath)
}
