package api

type LoopData struct {
	Count       int                    `json:"count,omitempty"`
	Settings    map[string]interface{} `json:"settings,omitempty"`
	Actions     map[string]string      `json:"actions,omitempty"`
	Translation map[string]string      `json:"translation,omitempty"`
}
