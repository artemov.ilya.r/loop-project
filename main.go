package main

import (
	"github.com/joho/godotenv"
	"log"
	"os"
	. "system/api"
	. "system/db"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	databasePath := os.Getenv("DB_PATH")
	GetConnection(databasePath)
	RunAPI()
}
